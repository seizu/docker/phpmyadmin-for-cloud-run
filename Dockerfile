FROM phpmyadmin/phpmyadmin

RUN sed -i 's/80/${PORT}/g' /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf

# entrypointでcatがなぜか死ぬための回避策
RUN echo "<?php \$cfg['blowfish_secret'] = '%7Z}>^?Y!y4+U3_qkf;fX;&ggR+@Qh==';" > /etc/phpmyadmin/config.secret.inc.php

RUN sed -i '2iecho "listen port = ${PORT}"' /docker-entrypoint.sh